images = $(notdir $(wildcard ./images/*))
build-image = podman build --tag $(image):latest ./images/$(image)

test: build-images
	nextflow test.nf

build-images:
	$(foreach image, $(images), $(build-image);)

.PHONY: build-images test
