# Semantic Nextflow

## Installation

Download the jar file from https://commons.apache.org/proper/commons-io/download_io.cgi and put it into the lib folder.

In some cases it is necessary to give the current user the rights to set hardware limits in podman: https://rootlesscontaine.rs/getting-started/common/cgroup2/#enabling-cpu-cpuset-and-io-delegation

## Conventions

1. `params` should come before the process where they are used.
2. List all possible values in a comment and set a default value.

   ```groovy
   #!/usr/bin/env nextflow

   params.reasoner = 'hermit' // hermit jfact whelk emr structural
   ```

3. camelCase for variables and CamelCase for channels, processes and workflows.
4. Use `''`.
