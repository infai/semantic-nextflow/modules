import java.io.File
import org.apache.commons.io.FileUtils

class TestUtils {
    static compareFileContent = { output, expected ->
        FileUtils.contentEquals(
            new File(output.toString()),
            new File(expected.toString())
        )
    }
}
