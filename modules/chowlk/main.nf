#!/usr/bin/env nextflow

process ConvertDiagramToOntology {
    container 'localhost/chowlk:latest'

    input:
        path chowlk_diagram

    output:
        path '*.ttl', emit: ontology

    """
    converter $chowlk_diagram ${chowlk_diagram.getBaseName()}.ttl --type ontology --format ttl
    """
}
