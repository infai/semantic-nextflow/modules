#!/usr/bin/env nextflow

params.apiToken = ''
params.url = ''

process CkanCreateResource {
    container 'localhost/utils:latest'
    maxForks 1

    input:
        path uploadFile
        val ownerOrg
        val packageId

    """
    curl \\
        -H'Authorization: $params.apiToken' \\
        '$params.url/api/action/package_create' \\
        --form name=$packageId \\
        --form owner_org=$ownerOrg
    curl --fail-with-body \\
        -H'Authorization: $params.apiToken' \\
        '$params.url/api/action/resource_create' \\
        --form name=$uploadFile \\
        --form owner_org=$ownerOrg \\
        --form upload=@$uploadFile \\
        --form package_id=$packageId \\
        --form format=${uploadFile.getExtension()}
    """
}
