#!/usr/bin/env nextflow

params.method = 'PUT' // PUT POST
params.url = ''
params.user = ''
params.password = ''

if (params.user.isEmpty() && params.user.isEmpty())
    auth = ''
else
    auth = '--basic --user ' + params.user + ':' + params.password

process FusekiUploadDataset {
    container 'localhost/utils:latest'

    input:
        path dataset
        val databaseName

    """
    curl \\
        $auth \\
        -X POST '$params.url/\$/datasets' \\
        --data 'dbType=tdb2' \\
        --data 'dbName=$databaseName'
    curl --fail-with-body \\
        $auth \\
        --location \\
        --request $params.method '$params.url/$databaseName/data' \\
        --header 'Content-Type: multipart/form-data' \\
        --form 'file.${dataset.getExtension()}=@$dataset'
    """
}
