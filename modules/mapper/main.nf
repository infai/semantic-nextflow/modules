#!/usr/bin/env nextflow

process MapYarrrmlToRml {
    container 'localhost/mapper-yarrrml:latest'

    input:
        path yarrrmlMapping

    output:
        path '*.ttl', emit: rmlMapping

    """
    yarrrml-parser -i $yarrrmlMapping -o ${yarrrmlMapping.getBaseName()}.rml.ttl
    """
}

params.serialization = 'nquads' // nquads turtle trig trix jsonld hdt

fileExtensions = [
            'nquads':'nq',
            'turtle':'ttl',
            'trig':'trig',
            'trix':'xml',
            'jsonld':'jsonld',
            'hdt':'hdt'
]

process MapToRdfwithRml {
    container 'localhost/mapper-rml:latest'

    input:
        path rmlMappings
        path sources

    output:
        path outputFile, emit: graph

    script:

    outputFile = 'graph.' + fileExtensions[params.serialization]

    """
    java -jar /rmlmapper.jar \\
        -o ./$outputFile \\
        --serialization $params.serialization \\
        --mappingfile $rmlMappings
    """
}

workflow MapToRdf {
    take: tupleMappingsSources

    main:
        input = tupleMappingsSources.multiMap { it ->
            mappings: it[0]
            sources: it[1]
        }
        mappings = input.mappings
            .flatten()
            .branch {
                yarrrml: it.getExtension() =~ '(yarrrml|yml|yaml)'
                rml: true
        }

        MapToRdfwithRml(
            MapYarrrmlToRml(mappings.yarrrml).mix(mappings.rml),
            input.sources
        )
    emit:
        graph = MapToRdfwithRml.out.graph
}
