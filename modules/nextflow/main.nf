#!/usr/bin/env nextflow

params.outputDir

process NextflowPublish {

    publishDir params.outputDir, mode: 'copy'

    input:
        path inputs

    output:
        path inputs

    """
    """
}
