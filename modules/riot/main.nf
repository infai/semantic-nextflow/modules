#!/usr/bin/env nextflow

// jsonld n3 nquads ntriples rdfjson rdfthrift rdfxml trig trix turtle
params.serialization = 'turtle'
params.formatted = true

fileExtensions = [
    'jsonld':'jsonld',
    'n3': 'n3',
    'nquads':'nq',
    'ntriples':'nt',
    'rdfjson':'rj',
    'rdfthrift': 'rt',
    'rdfxml': 'rdf',
    'trig':'trig',
    'trix':'xml',
    'turtle':'ttl'
]
fileExtension = fileExtensions[params.serialization]
format = params.formatted ? 'formatted' : 'stream'

process MergeRdf {
    container 'docker.io/stain/jena:4.0.0'

    input:
        path rdf

    output:
        path 'merged.*', emit: mergedRdf

    """
    riot --quiet --$format=$params.serialization * > merged.$fileExtension
    """
}
