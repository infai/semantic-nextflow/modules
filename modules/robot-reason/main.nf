#!/usr/bin/env nextflow

params.reasoner = 'hermit' // hermit jfact whelk emr structural
params.axiomGenerators = [
        SubClass: true,
        EquivalentClass: true,
        DisjointClasses: true,

        DataPropertyCharacteristic: true,
        EquivalentDataProperties: true,
        SubDataProperty: true,

        ClassAssertion: true,
        PropertyAssertion: true,

        EquivalentObjectProperty: true,
        InverseObjectProperties: true,
        ObjectPropertyCharacteristic: true,
        SubObjectProperty: true,
        ObjectPropertyRange: true,
        ObjectPropertyDomain: true,
    ]

axiomGenerators = params.axiomGenerators
    .findAll { it.value == true }
    .collect { it -> it.key }
    .join(' ')

process RobotReason {
    container 'docker.io/obolibrary/robot:v1.8.1'

    input:
        path graph

    output:
        path '*.owl', emit: inferredAxioms

    """
    robot reason \\
        --input $graph \\
        --reasoner $params.reasoner \\
        --include-indirect true \\
        --axiom-generators "$axiomGenerators" \\
        reduce --output reasoned.owl
    """
}
