#!/usr/bin/env nextflow

process executeSHACL {
    container 'docker.io/python:latest'

    input:
      path rdfGraph
      path shaclShapes

    output:
      path 'validation-report.ttl', emit: validationResultTtl


    """
    pyshacl --allow-warnings --shacl "$shaclShapes" --format turtle --output "validation-report.ttl" "$rdfGraph"
    """
}
