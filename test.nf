#!/usr/bin/env nextflow

include { TestMapper } from './tests/modules/mapper/test.nf'
include { TestSHACL } from './tests/modules/shacl/test.nf'

workflow {
    TestMapper()
    TestSHACL()
}
