#!/usr/bin/env nextflow

include { MapToRdf } from '../../../modules/mapper/main.nf'

workflow TestYarrrmlMapping {
    MapToRdf(Channel.of(
        [yarrrmlMapping, yarrrmlInput]
    )).subscribe {
        assert TestUtils.compareFileContent(it, yarrrmlExpected)
    }
}

workflow TestRmlMapping {
    MapToRdf(Channel.of(
        [rmlMapping, rmlInput]
    )).subscribe {
        assert TestUtils.compareFileContent(it, rmlExpected)
    }
}

workflow TestMultipleMappings {
    MapToRdf(Channel.of(
        [rmlMapping, rmlInput],
        [yarrrmlMapping, yarrrmlInput]
    )).first().subscribe {
        assert TestUtils.compareFileContent(it, rmlExpected)
    }
    MapToRdf.out.last().subscribe {
        assert TestUtils.compareFileContent(it, yarrrmlExpected)
    }
}

yarrrmlMapping = moduleDir + '/resources/yarrrml/mapping.yarrrml'
yarrrmlInput = moduleDir + '/resources/yarrrml/input.txt'
yarrrmlExpected = moduleDir + '/resources/yarrrml/expected.nq'
rmlMapping = moduleDir + '/resources/rml/mapping.ttl'
rmlInput = moduleDir + '/resources/rml/input.csv'
rmlExpected = moduleDir + '/resources/rml/expected.nq'

workflow TestMapper {
    TestYarrrmlMapping()
    TestRmlMapping()
    TestMultipleMappings()
}
