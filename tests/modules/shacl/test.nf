#!/usr/bin/env nextflow

include { executeSHACL } from '../../../modules/shacl/main.nf'

workflow TestSHACL {
    executeSHACL(
        Channel.fromPath(rdfGraph),
        Channel.fromPath(shaclShapes)
    ).subscribe {
        assert TestUtils.compareFileContent(it, rdfExpected)
    }
}


rdfGraph = moduleDir + '/resources/data.ttl'
shaclShapes = moduleDir + '/resources/shapes.ttl'
rdfExpected = moduleDir + '/resources/expected.ttl'
